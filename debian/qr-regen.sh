#!/bin/sh -e
# https://github.com/pymumu/smartdns/issues/452

# zbar-tools
# qrencode

# Q: how to re-generate the URLs ?
# A: rm debian/*.txt; bash debian/qr-regen.sh

U1="https://github.com/pymumu/smartdns/raw/master/doc/alipay_donate.jpg"
U2="https://github.com/pymumu/smartdns/raw/master/doc/wechat_donate.jpg"

if ! test -r debian/u1.txt; then
	wget -c ${U1} -O u1.jpg
	zbarimg -q u1.jpg | sed -e 's/QR-Code://' > debian/u1.txt
fi
if ! test -r debian/u2.txt; then
	wget -c ${U2} -O u2.jpg
	zbarimg -q u2.jpg | sed -e 's/QR-Code://' > debian/u2.txt
fi

qrencode -lH -o alipay_donate.jpg < debian/u1.txt
qrencode -lH -o wechat_donate.jpg < debian/u2.txt
