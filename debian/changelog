smartdns (46+dfsg-1) unstable; urgency=medium

  * New upstream version 46+dfsg

 -- Mo Zhou <lumin@debian.org>  Wed, 03 Jul 2024 02:51:02 -0400

smartdns (45+dfsg-1) unstable; urgency=medium

  * New upstream version 45+dfsg
  * Rebase existing patches.
  * Update copyright information.

 -- Mo Zhou <lumin@debian.org>  Thu, 15 Feb 2024 14:13:09 -0500

smartdns (43+dfsg-1) unstable; urgency=medium

  * New upstream version 43+dfsg
  * Refresh existing patches.
  * Add clean target to solve twice in a row FTBFS. (Closes: #1047008)

 -- Mo Zhou <lumin@debian.org>  Fri, 18 Aug 2023 18:34:06 -0400

smartdns (42+dfsg-1) unstable; urgency=medium

  * New upstream version 42+dfsg
  * Rebase existing patches.

 -- Mo Zhou <lumin@debian.org>  Thu, 13 Jul 2023 21:39:03 -0700

smartdns (40+dfsg-1) unstable; urgency=medium

  * New upstream version 40+dfsg
  * Rebase existing patches.

 -- Mo Zhou <lumin@debian.org>  Fri, 17 Feb 2023 16:38:29 -0500

smartdns (39+dfsg-1) unstable; urgency=medium

  * New upstream version 39+dfsg
  * Rebase existing patches.

 -- Mo Zhou <lumin@debian.org>  Sun, 18 Dec 2022 11:21:51 -0500

smartdns (38.1+dfsg-1) unstable; urgency=medium

  * New upstream version 38.1+dfsg
  * Refresh existing patches.
  * Add lintian overrides.

 -- Mo Zhou <lumin@debian.org>  Fri, 28 Oct 2022 13:43:00 -0400

smartdns (36.1+dfsg-1) unstable; urgency=medium

  * New upstream version 36.1+dfsg

 -- Mo Zhou <lumin@debian.org>  Fri, 08 Apr 2022 14:57:02 -0400

smartdns (36+dfsg-1) unstable; urgency=medium

  * New upstream version 36+dfsg

 -- Mo Zhou <lumin@debian.org>  Sun, 03 Apr 2022 19:58:47 -0400

smartdns (35+dfsg-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.5.1, no changes needed.

  [ Mo Zhou ]
  * New upstream version 35+dfsg
  * Rebase existing patches and remove merged systemd unit patch.

 -- Mo Zhou <lumin@debian.org>  Thu, 06 Jan 2022 10:12:34 -0500

smartdns (33+dfsg-2.1) unstable; urgency=medium

  * Non-maintainer upload.
    ACK by maintainer.
  * Backport patch to fix TimeoutStopSec key in systemd unit file
    (Closes: #989072)

 -- Shengjing Zhu <zhsj@debian.org>  Fri, 28 May 2021 00:37:23 +0800

smartdns (33+dfsg-2) unstable; urgency=medium

  [ Chris Lamb ]
  * Make the build reproducible (Closes: #970908)

 -- Mo Zhou <lumin@debian.org>  Wed, 02 Dec 2020 19:57:30 +0800

smartdns (33+dfsg-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * autopkgtest: call command with explict sbin path

 -- Aron Xu <aron@debian.org>  Mon, 28 Sep 2020 15:45:04 +0800

smartdns (33+dfsg-1) unstable; urgency=medium

  * New upstream version 33+dfsg
  * Mark autopkgtest case as superficial.
  * Upload to unstable.

 -- Mo Zhou <lumin@debian.org>  Thu, 24 Sep 2020 10:30:12 +0800

smartdns (32+dfsg-1~exp1) experimental; urgency=medium

  * New upstream version 32+dfsg

 -- Mo Zhou <lumin@debian.org>  Wed, 02 Sep 2020 07:54:08 +0800

smartdns (31+dfsg-1~exp2) experimental; urgency=medium

  * Update the build target to fix the missing systemd service.

 -- Mo Zhou <lumin@debian.org>  Wed, 27 May 2020 02:37:06 +0000

smartdns (31+dfsg-1~exp1) experimental; urgency=medium

  * New upstream version 31+dfsg
  * Refresh URL in debian/u1.txt
  * Apply wrap-and-sort.
  * Bump debhelper compat level to 13 (no change).
  * Add a usage hint in debian/qr-regen.sh
  * Add dversionmangle in d/watch.
  * Fix file paths in d/copyright.
  * Add autopkgtest control file.

 -- Mo Zhou <lumin@debian.org>  Tue, 26 May 2020 15:10:09 +0800

smartdns (30+dfsg-1~exp1) experimental; urgency=medium

  * Initial release. (Closes: #951307)

 -- Mo Zhou <lumin@debian.org>  Wed, 01 Apr 2020 20:12:03 +0800
